package com.github.iam.eventbus.events;

public class AccountActivatedEvent {
    private final String userName;

    public AccountActivatedEvent(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
