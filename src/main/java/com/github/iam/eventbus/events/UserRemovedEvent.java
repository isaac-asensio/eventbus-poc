package com.github.iam.eventbus.events;

public class UserRemovedEvent {
    private final String email;

    public UserRemovedEvent(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
