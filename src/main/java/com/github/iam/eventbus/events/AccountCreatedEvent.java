package com.github.iam.eventbus.events;

public class AccountCreatedEvent {

    private String userName;

    public AccountCreatedEvent(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
