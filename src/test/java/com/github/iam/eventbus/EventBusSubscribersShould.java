package com.github.iam.eventbus;

import com.github.iam.eventbus.events.AccountActivatedEvent;
import com.github.iam.eventbus.events.AccountCreatedEvent;
import com.github.iam.eventbus.events.UserRemovedEvent;
import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventBusSubscribersShould {


    public static final String JOHN_DOE_USERNAME = "john.doe";
    private EventBus eventBus;
    @Spy private EmailSubscriber emailSubscriber;
    @Spy private SmsSubscriber smsSubscriber;

    @Before
    public void setUp() throws Exception {

        eventBus = new EventBus();

        eventBus.register(emailSubscriber);
        eventBus.register(smsSubscriber);

    }

    @Test
    public void sendAnEmailButAnSmsWhenAnAccountCreatedEventIsSent() {

        eventBus.post(new AccountCreatedEvent(JOHN_DOE_USERNAME));

        verifyZeroInteractions(smsSubscriber);
        verify(emailSubscriber).handle(any(AccountCreatedEvent.class));
    }

    @Test
    public void sendAndEmailAndSmsWhenAnAccountIsActivated() {

        eventBus.post(new AccountActivatedEvent(JOHN_DOE_USERNAME));

        verify(emailSubscriber).handle(any(AccountActivatedEvent.class));
        verify(smsSubscriber).handle(any(AccountActivatedEvent.class));

    }

    @Test
    public void doNothingWhenAnyoneIsSubscribingToAnEvent() {

        eventBus.post(new UserRemovedEvent(JOHN_DOE_USERNAME));

        verifyZeroInteractions(emailSubscriber);
        verifyZeroInteractions(smsSubscriber);
    }
}
