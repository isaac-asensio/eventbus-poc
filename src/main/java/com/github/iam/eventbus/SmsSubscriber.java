package com.github.iam.eventbus;

import com.github.iam.eventbus.events.AccountActivatedEvent;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmsSubscriber {

    private final static Logger LOGGER = LoggerFactory.getLogger(SmsSubscriber.class);

    @Subscribe
    public void handle(AccountActivatedEvent event) {
        LOGGER.info("Account activated: sending a SMS to user {}", event.getUserName());
    }

}
