package com.github.iam.eventbus;

import com.github.iam.eventbus.events.AccountActivatedEvent;
import com.github.iam.eventbus.events.AccountCreatedEvent;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailSubscriber {

    private final static Logger LOGGER = LoggerFactory.getLogger(EmailSubscriber.class);

    @Subscribe
    public void handle(AccountCreatedEvent event) {
        LOGGER.info("Account created: sending an activation email to {}", event.getUserName());
    }

    @Subscribe
    public void handle(AccountActivatedEvent event) {
        LOGGER.info("Account activated: sending a welcome email to {}", event.getUserName());
    }
}
